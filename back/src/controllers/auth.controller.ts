import { Request, Response } from "../types/types";
import { initializeClient, closeClient } from "../models/db";

export const test = async (req: Request, res: Response) => {
  try {
    const client = await initializeClient();

    const query = "SELECT * FROM users";

    const result = await client.query(query);

    if (result.rows.length > 0) {
      res.status(200).json({ message: result.rows[0] });
    } else {
      res.status(200).json({ message: "No user found." });
    }
  } catch (err: any) {
    res.status(500).json({ message: err.message });
  } finally {
    await closeClient();
  }
};
