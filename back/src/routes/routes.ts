import express, { Request, Response } from "express";
import { Router } from "../types/types";
import { test } from "../controllers/auth.controller";

const router: Router = express.Router();

// Welcome to my api
router.get("/", (req: Request, res: Response) => {
  res.send("Welcome to my API");
});

router.get("/test", test);

export default router;
