import { Client } from "pg";
import dotenv from "dotenv";

dotenv.config();

const uri: string = `postgresql://${process.env.PG_USER}:${process.env.PG_PASSWORD}@${process.env.PG_HOST}:${process.env.PG_PORT}/${process.env.PG_DATABASE}`;

const client = new Client({
  connectionString: uri,
});

export const initializeClient = async (): Promise<Client> => {
  try {
    await client.connect();
    console.log("Connected to PostgreSQL");
    return client;
  } catch (error: any) {
    console.error("Failed to connect to PostgreSQL:", error.message);
    throw error;
  }
};

export const closeClient = async (): Promise<void> => {
  try {
    await client.end();
    console.log("Disconnected from PostgreSQL");
  } catch (error: any) {
    console.error("Failed to disconnect from PostgreSQL:", error.message);
  }
};

